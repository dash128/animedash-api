const SECRET_TOKEN = 'MY_KEY_IS_M_OF_MOTHER'
const PORT = 3000
const MONGO_URI = 'mongodb://localhost/animedb'

export {
    PORT,
    MONGO_URI,
    SECRET_TOKEN
}